package SSAAmpli;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.*;
import fr.esrf.tangoatk.core.attribute.DevStateSpectrum;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class AlarmItem {
  String   state;
  boolean  isEnabled;
  AlarmItem() {
    state = IDevice.UNKNOWN;
    isEnabled = true;
  }
}

class AlarmTableCellRenderer extends JPanel implements TableCellRenderer {

  JCheckBox check;
  JLabel    state;

  AlarmTableCellRenderer() {

    setLayout(new BorderLayout());
    check = new JCheckBox("");
    add(check,BorderLayout.WEST);
    state = new JLabel();
    state.setOpaque(true);
    add(state,BorderLayout.CENTER);

  }

  public Component getTableCellRendererComponent(JTable table,Object value,
                                                 boolean isSelected, boolean hasFocus, int row, int column) {
    AlarmItem it = (AlarmItem)value;
    check.setSelected(it.isEnabled);
    state.setBackground(ATKConstant.getColor4State(it.state));
    return this;
  }

}

class AlarmTableCellEditor extends AbstractCellEditor implements TableCellEditor {

  AlarmTableCellRenderer renderer;

  public AlarmTableCellEditor() {
    renderer = new AlarmTableCellRenderer();
  }

  public Object getCellEditorValue() {
    AlarmItem ret = new AlarmItem();
    ret.isEnabled = renderer.check.isSelected();
    return ret;
  }

  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
                                               int row, int column) {
    AlarmItem it = (AlarmItem)value;
    renderer.check.setSelected(it.isEnabled);
    renderer.state.setBackground(ATKConstant.getColor4State(it.state));
    return renderer;
  }

}


public class TransistorAlarmDialog extends JFrame implements ActionListener,IDevStateSpectrumListener {

  private AttributeList     attList;
  private String            towerName;
  private String            towerAlarmName;
  private JTable            theTable;
  private DefaultTableModel theModel;
  private AlarmTableCellEditor myCellEditor;
  private AlarmTableCellRenderer myCellRenderer;
  private JScrollPane       theScroll;
  private JPanel            innerPanel;
  private JPanel            btnPanel;
  private JButton           dismissBtn;
  private JButton           enableAllBtn;
  private DevStateSpectrum  stateModel;
  private Object[][]        cellObjects;
  private DeviceProxy       alarmsDS;
  private int               nbColdPlate;
  private ErrorHistory      errWin;

  public TransistorAlarmDialog() {
  }

  public TransistorAlarmDialog(ErrorHistory errWin,boolean isSR,String ampName) {
    this.errWin = errWin;
    initComponents(isSR,ampName);
  }
  
  public void initComponents(boolean isSR,String ampName) {

    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        exitPanel();
      }
    });

    innerPanel = new JPanel();
    innerPanel.setPreferredSize(new Dimension(800,190));
    innerPanel.setLayout(new BorderLayout());

    if( isSR ) {
      towerName = "srrf/ssa-tower/"+ampName;
      towerAlarmName = "srrf/ssa-alarm/"+ampName;
    } else {
      towerName = "sy/rfssa-tower/"+ampName;
      towerAlarmName = "sy/rfssa-alarm/"+ampName;
    }
    nbColdPlate = 8;
    String ampNameSpace = ampName.substring(0,ampName.length()-1) + " " +
                          ampName.substring(ampName.length()-1,ampName.length());

    theModel = new DefaultTableModel()  {
      public Class getColumnClass(int columnIndex) {
        if(columnIndex>0)
          return AlarmItem.class;
        else
          return String.class;
      }
      public boolean isCellEditable(int row, int column) {
        return column>0;
      }
      public void setValueAt(Object aValue, int row, int column) {

        AlarmItem src = (AlarmItem)aValue;
        AlarmItem dst = (AlarmItem)cellObjects[row][column];

        if(src.isEnabled!=dst.isEnabled) {
          if( SetAlarm(row,column-1,src.isEnabled) )
            dst.isEnabled = src.isEnabled;
        }

      }
    };

    String[] colNames = {"Cold plate","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"};

    cellObjects = new Object[nbColdPlate][17];
    for (int i = 0; i < nbColdPlate; i++) {
      cellObjects[i][0]=ampNameSpace+"-"+(i+2);
      for(int j=0;j<16;j++)
        cellObjects[i][j+1] = new AlarmItem();
    }

    theModel.setDataVector(cellObjects,colNames);

    myCellEditor = new AlarmTableCellEditor();
    myCellEditor.renderer.check.addActionListener(this);
    myCellRenderer = new AlarmTableCellRenderer();

    theTable = new JTable(theModel);
    theTable.setDefaultRenderer(AlarmItem.class,myCellRenderer);
    theTable.setDefaultEditor(AlarmItem.class,myCellEditor);

    theScroll = new JScrollPane(theTable);
    theTable.getColumnModel().getColumn(0).setMinWidth(90);

    innerPanel.add(theScroll,BorderLayout.CENTER);


    btnPanel = new JPanel();
    btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

    enableAllBtn = new JButton("Enable All");
    enableAllBtn.addActionListener(this);
    btnPanel.add(enableAllBtn);

    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    btnPanel.add(dismissBtn);

    innerPanel.add(btnPanel,BorderLayout.SOUTH);


    // Connect to device
    attList = new AttributeList();
    attList.addErrorListener(errWin);

    try {
      stateModel = (DevStateSpectrum)attList.add(towerName+"/TransistorSates");
      stateModel.addDevStateSpectrumListener(this);
    } catch (ConnectionException e) {}

    attList.setRefreshInterval(2000);
    attList.startRefresher();

    try {
      alarmsDS = new DeviceProxy(towerAlarmName);
      refreshAlarmState();
    } catch (DevFailed e) {
      ErrorPane.showErrorMessage(this,towerAlarmName,e);
    }

    setContentPane(innerPanel);
    setTitle("RF SSA Transistor Alarm Inhibition [" + ampName + "]");
    ATKGraphicsUtils.centerFrameOnScreen(this);
    setVisible(true);

  }

  private void refreshAlarmState() throws DevFailed {

    DeviceAttribute da = alarmsDS.read_attribute("DisabledTransistors");
    short[] values = da.extractShortArray();
    for(int i=0;i<nbColdPlate;i++) {
      int us = values[i];
      us = us & 0xFFFF;
      int mask = 1;
      for(int j=0;j<16;j++) {
        AlarmItem it = (AlarmItem)cellObjects[i][j+1];
        it.isEnabled = ((us&mask)==0);
        mask = mask << 1;
      }
    }
    theModel.fireTableDataChanged();

  }

  private boolean SetAlarm(int cpIdx,int trIdx,boolean enable) {

    try {

      DeviceData da = new DeviceData();
      short[] argin = new short[]{(short)cpIdx, (short)trIdx};
      da.insert(argin);
      if (enable)
        alarmsDS.command_inout("EnableAlarm", da);
      else
        alarmsDS.command_inout("DisableAlarm", da);

    } catch (DevFailed e) {

      ErrorPane.showErrorMessage(this, towerAlarmName, e);
      return false;

    }

    return true;

  }

  public void devStateSpectrumChange(DevStateSpectrumEvent evt) {

    String[] states = evt.getValue();

    for(int i=0;i<nbColdPlate*16;i++) {
      int row = i/16;
      int col = i%16;
      AlarmItem it = (AlarmItem)cellObjects[row][col+1];
      it.state = states[i];
    }
    theModel.fireTableDataChanged();

  }

  public void stateChange(AttributeStateEvent evt) {
  }

  public void errorChange(ErrorEvent evt) {
  }

  public void actionPerformed(ActionEvent evt) {

    Object src = evt.getSource();

    if(src==dismissBtn) {
      setVisible(false);
    } else if( src==myCellEditor.renderer.check ) {
      myCellEditor.stopCellEditing();
    } else if( src==enableAllBtn ) {
      try {
        alarmsDS.command_inout("Reset");
        for(int i=0;i<nbColdPlate;i++)
          for(int j=0;j<16;j++)
            ((AlarmItem)cellObjects[i][j+1]).isEnabled = true;
        theModel.fireTableDataChanged();
      } catch (DevFailed e) {
        ErrorPane.showErrorMessage(this, towerAlarmName, e);
      }
    }

  }

  private void exitPanel() {

    if(stateModel!=null) stateModel.removeDevStateSpectrumListener(this);

    attList.stopRefresher();
    attList.removeErrorListener(errWin);
    attList.clear();
    attList = null;
    //cmdList.removeErrorListener(errWin);
    //cmdList.clear();
    //cmdList = null;

    dispose();

  }


}
