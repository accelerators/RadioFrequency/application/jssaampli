/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SSAAmpli;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.attribute.DevStateScalar;
import fr.esrf.tangoatk.core.attribute.StringScalar;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.jdraw.JDSwingObject;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author pons
 */
public class MainPanel extends javax.swing.JFrame {
  
  private final static String APP_RELEASE = "2.4";

  static ErrorHistory errWin = null;


  private boolean runningFromShell;

  private CommandList cmdList;
  private AttributeList attList;

  private RFPowerDialog rfPowerDialog=null;

  private String  devName;
  private String  traName;
  boolean isSR;

  public MainPanel(String traName) {
    this(traName,false);
  }

  /**
   * Creates new form MainPanel
   */
  public MainPanel(String traName,boolean runningFromShell) {

    this.runningFromShell = runningFromShell;
    this.traName = traName;
    
    if( traName.toLowerCase().startsWith("tra0") ) {
      devName = "sy/rfssa-ampli/" + traName;
      isSR = false;
    } else {
      devName = "srrf/ssa-ampli/" + traName;
      isSR = true;
    }

    // Handle windowClosing (Close from the system menu)
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        exitForm();
      }
    });

    if( errWin==null ) errWin = new ErrorHistory();
    
    initComponents();
    
    cmdList = new CommandList();
    cmdList.addErrorListener(ErrorPopup.getInstance());
    cmdList.addErrorListener(errWin);
    
    attList = new AttributeList();
    attList.addErrorListener(ErrorPopup.getInstance());
    
    // Global commands
    try {
      
      VoidVoidCommand onCmd = (VoidVoidCommand)cmdList.add(devName+"/On");
      onCommandViewer.setModel(onCmd);
      VoidVoidCommand offCmd = (VoidVoidCommand)cmdList.add(devName+"/Off");
      offCommandViewer.setModel(offCmd);
      VoidVoidCommand standbyCmd = (VoidVoidCommand)cmdList.add(devName+"/Standby");
      standbyCommandViewer.setModel(standbyCmd);
      VoidVoidCommand resetCmd = (VoidVoidCommand)cmdList.add(devName+"/Reset");      
      resetCommandViewer.setModel(resetCmd);
      
      DevStateScalar stateModel = (DevStateScalar)attList.add(devName+"/State");
      StringScalar statusModel = (StringScalar)attList.add(devName+"/ShortStatus");
      stateStatusViewer.setStateModel(stateModel);
      stateStatusViewer.setStatusModel(statusModel);
      
    } catch(ConnectionException e) {      
    }
        
    // Loads the synoptic    
    String synopticName = "ssaampli-"+traName+".jdw";
    InputStream jdFileInStream = this.getClass().getResourceAsStream("/SSAAmpli/"+synopticName);

    if (jdFileInStream == null) {
      JOptionPane.showMessageDialog(
              null, "Failed to get the inputStream for the synoptic file resource : " + synopticName + " \n\n",
              "Resource error",
              JOptionPane.ERROR_MESSAGE);
      exitForm();
    }

    InputStreamReader inStrReader = new InputStreamReader(jdFileInStream);
    try {
      theSynoptic.setErrorHistoryWindow(errWin);
      theSynoptic.setToolTipMode(TangoSynopticHandler.TOOL_TIP_NAME);
      theSynoptic.setAutoZoom(true);
      theSynoptic.loadSynopticFromStream(inStrReader);
    } catch (IOException ioex) {
      JOptionPane.showMessageDialog(
              null, "Cannot find load the synoptic input stream reader.\n" + " Application will abort ...\n" + ioex,
              "Resource access failed",
              JOptionPane.ERROR_MESSAGE);
      exitForm();
    }
    
    initTransistorAlarms();
    
    attList.setRefreshInterval(1000);
    attList.startRefresher();

    setTitle("SSA Amplifier " + APP_RELEASE);
    ATKGraphicsUtils.centerFrameOnScreen(this);
    setVisible(true);
  }
  
  public void showDiagPanel() {

    RFDiagDialog dlg = new RFDiagDialog(devName);
    ATKGraphicsUtils.centerFrameOnScreen(dlg);
    dlg.showDialog();

  }

  private void initTransistorAlarms() {

    final String[] names;
    final String twName;
    final String twAlarmName;
    final MainPanel self = this;

    names = new String[]{ traName+"M",traName+"S" };

    if( isSR ) {
      // SR
      twName = "srrf/ssa-tower/";
      twAlarmName = "srrf/ssa-alarm/";
    } else {
      // SYRF
      twName = "sy/rfssa-tower/";
      twAlarmName = "sy/rfssa-alarm/";
    }

    // SYRF
    for (int i = 0; i < names.length; i++) {

      Vector v;
      SimpleScalarViewer sv;
      JDSwingObject obj;
      final int idx = i;
      final MouseAdapter mA = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          TransistorAlarmDialog dlg = new TransistorAlarmDialog(errWin,isSR,names[idx]);
        }
      };

      v = theSynoptic.getObjectsByName(twAlarmName + names[i] + "/DisabledTransistorCount", false);
      obj = (JDSwingObject) v.get(0);
      sv = (SimpleScalarViewer) obj.getComponent();
      sv.setEditable(false);
      sv.addMouseListener(mA);

      v = theSynoptic.getObjectsByName(twName + names[i] + "/TransistorFailures", false);
      obj = (JDSwingObject) v.get(0);
      sv = (SimpleScalarViewer) obj.getComponent();
      sv.setEditable(false);
      sv.addMouseListener(mA);

    }

  }

  private void clearModel() {

    theSynoptic.clearSynopticFileModel();
    cmdList.removeErrorListener(errWin);
    cmdList.removeErrorListener(ErrorPopup.getInstance());
    attList.removeErrorListener(errWin);
    offCommandViewer.setModel((ICommand)null);
    standbyCommandViewer.setModel((ICommand)null);
    onCommandViewer.setModel((ICommand)null);
    resetCommandViewer.setModel((ICommand)null);    
    if( rfPowerDialog!=null ) rfPowerDialog.clearModel();
    attList.stopRefresher();
    attList.clear();
    cmdList.clear();

  }

  

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {
    java.awt.GridBagConstraints gridBagConstraints;

    centerPanel = new javax.swing.JPanel();
    theSynoptic = new fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer();
    downPanel = new javax.swing.JPanel();
    stateStatusViewer = new fr.esrf.tangoatk.widget.attribute.StateStatusViewer();
    offCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
    standbyCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
    onCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
    resetCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
    menuBar = new javax.swing.JMenuBar();
    fileMenu = new javax.swing.JMenu();
    exitMenuItem = new javax.swing.JMenuItem();
    viewMenu = new javax.swing.JMenu();
    rfDiagMenuItem = new javax.swing.JMenuItem();
    rfPowerMenuItem = new javax.swing.JMenuItem();
    jSeparator1 = new javax.swing.JPopupMenu.Separator();
    diagMenuItem = new javax.swing.JMenuItem();
    errorMenuItem = new javax.swing.JMenuItem();

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    centerPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    centerPanel.setLayout(new java.awt.GridLayout());
    centerPanel.add(theSynoptic);

    getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);

    downPanel.setLayout(new java.awt.GridBagLayout());
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.weightx = 1.0;
    gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 0);
    downPanel.add(stateStatusViewer, gridBagConstraints);

    offCommandViewer.setText("Off");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 0);
    downPanel.add(offCommandViewer, gridBagConstraints);

    standbyCommandViewer.setText("Standby");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 0);
    downPanel.add(standbyCommandViewer, gridBagConstraints);

    onCommandViewer.setText("On");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 3;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 0);
    downPanel.add(onCommandViewer, gridBagConstraints);

    resetCommandViewer.setText("Reset");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 4;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
    downPanel.add(resetCommandViewer, gridBagConstraints);

    getContentPane().add(downPanel, java.awt.BorderLayout.SOUTH);

    fileMenu.setText("File");

    exitMenuItem.setText("Exit");
    exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        exitMenuItemActionPerformed(evt);
      }
    });
    fileMenu.add(exitMenuItem);

    menuBar.add(fileMenu);

    viewMenu.setText("View");

    rfDiagMenuItem.setText("RF Diagnostic Panel...");
    rfDiagMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        rfDiagMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(rfDiagMenuItem);

    rfPowerMenuItem.setText("RF Power panel...");
    rfPowerMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        rfPowerMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(rfPowerMenuItem);
    viewMenu.add(jSeparator1);

    diagMenuItem.setText("Diagnostics...");
    diagMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        diagMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(diagMenuItem);

    errorMenuItem.setText("Errors...");
    errorMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        errorMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(errorMenuItem);

    menuBar.add(viewMenu);

    setJMenuBar(menuBar);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
    exitForm();
  }//GEN-LAST:event_exitMenuItemActionPerformed

  private void rfDiagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rfDiagMenuItemActionPerformed
    showDiagPanel();
  }//GEN-LAST:event_rfDiagMenuItemActionPerformed

  private void rfPowerMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rfPowerMenuItemActionPerformed
    if( rfPowerDialog==null ) rfPowerDialog = new RFPowerDialog(devName);
    ATKGraphicsUtils.centerFrameOnScreen(rfPowerDialog);
    rfPowerDialog.showDialog();
  }//GEN-LAST:event_rfPowerMenuItemActionPerformed

  private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
    fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
  }//GEN-LAST:event_diagMenuItemActionPerformed

  private void errorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorMenuItemActionPerformed
    ATKGraphicsUtils.centerFrameOnScreen(errWin);
    errWin.setVisible(true);
  }//GEN-LAST:event_errorMenuItemActionPerformed

  /**
   * Exit the application
   */
  public void exitForm() {

    if (runningFromShell) {

      System.exit(0);

    } else {

      clearModel();
      setVisible(false);
      dispose();

    }

  }

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    
    if( args.length!=1 ) {
      System.out.println("Usage: jssaampli transmitter_name");
      System.out.println("Ex: jssaampli tra0-1");
      System.exit(0);
    }
    new MainPanel(args[0],true);
    
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPanel centerPanel;
  private javax.swing.JMenuItem diagMenuItem;
  private javax.swing.JPanel downPanel;
  private javax.swing.JMenuItem errorMenuItem;
  private javax.swing.JMenuItem exitMenuItem;
  private javax.swing.JMenu fileMenu;
  private javax.swing.JPopupMenu.Separator jSeparator1;
  private javax.swing.JMenuBar menuBar;
  private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer offCommandViewer;
  private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer onCommandViewer;
  private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer resetCommandViewer;
  private javax.swing.JMenuItem rfDiagMenuItem;
  private javax.swing.JMenuItem rfPowerMenuItem;
  private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer standbyCommandViewer;
  private fr.esrf.tangoatk.widget.attribute.StateStatusViewer stateStatusViewer;
  private fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer theSynoptic;
  private javax.swing.JMenu viewMenu;
  // End of variables declaration//GEN-END:variables
}
