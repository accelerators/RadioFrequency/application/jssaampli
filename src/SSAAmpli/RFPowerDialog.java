package SSAAmpli;

import fr.esrf.tangoatk.core.*;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RFPowerDialog extends JFrame implements ISpectrumListener,ActionListener {

  private JPanel innerPanel;
  private AttributeList      attList;

  private JPanel             driverPanel;
  private INumberSpectrum    driverSpectrumModel;
  private JTable             driverTable;
  private DefaultTableModel  driverModel;
  private String[]           driverColName;
  private JScrollPane        driverView;
  private Object[][]         driverInfo;

  private JPanel             fwpwPanel;
  private INumberSpectrum    fwpwSpectrumModel;
  private JTable             fwpwMTable;
  private DefaultTableModel  fwpwMModel;
  private Object[][]         fwpwMInfo;
  private String[]           fwpwMColName;
  private JScrollPane        fwpwMView;
  private JTable             fwpwSTable;
  private DefaultTableModel  fwpwSModel;
  private Object[][]         fwpwSInfo;
  private String[]           fwpwSColName;
  private JScrollPane        fwpwSView;

  private JPanel             repwPanel;
  private INumberSpectrum    repwSpectrumModel;
  private JTable             repwMTable;
  private DefaultTableModel  repwMModel;
  private Object[][]         repwMInfo;
  private String[]           repwMColName;
  private JScrollPane        repwMView;
  private JTable             repwSTable;
  private DefaultTableModel  repwSModel;
  private Object[][]         repwSInfo;
  private String[]           repwSColName;
  private JScrollPane        repwSView;

  private SimpleScalarViewer fwpwViewer;
  private SimpleScalarViewer repwViewer;

  private JButton            dismissBtn;

  RFPowerDialog(String devName) {

    innerPanel = new JPanel();
    innerPanel.setLayout(null);
    innerPanel.setPreferredSize(new Dimension(990,330));

    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    dismissBtn.setBounds(880,300,100,25);
    innerPanel.add(dismissBtn);

    // Compute domain and transmitter name
    String domain = devName.substring(0,devName.indexOf('/'));
    String traName = devName.substring(devName.lastIndexOf('/')+1);

    // Driver -------------------------------------------------------------------

    driverPanel = new JPanel();
    driverPanel.setLayout(null);
    driverPanel.setBorder(BorderFactory.createTitledBorder("Drivers"));
    driverPanel.setBounds(5,5,370,140);
    innerPanel.add(driverPanel);

    driverModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        return String.class;
      }
      public boolean isCellEditable(int row, int column) {
          return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
      }

    };

    driverTable = new JTable(driverModel);

    driverColName = new String[]{"" , "Current (A)" , "Temp (deg)" , "Load Temp (deg)" };
    driverInfo = new Object[5][4];
    driverInfo[0][0] = "Pre driver";
    driverInfo[1][0] = "M-UP";
    driverInfo[2][0] = "M-DOWN";
    driverInfo[3][0] = "S-UP";
    driverInfo[4][0] = "S-DOWN";
    driverModel.setDataVector(driverInfo, driverColName);

    driverView = new JScrollPane(driverTable);
    driverView.setBounds(5,20,355,101);

    driverPanel.add(driverView);

    // FwPw Panel ---------------------------------------------------------------

    fwpwPanel = new JPanel();
    fwpwPanel.setLayout(null);
    fwpwPanel.setBorder(BorderFactory.createTitledBorder("Forward power (KW)"));
    fwpwPanel.setBounds(380,5,610,140);
    innerPanel.add(fwpwPanel);

    fwpwMModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        return String.class;
      }
      public boolean isCellEditable(int row, int column) {
          return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
      }

    };

    fwpwMTable = new JTable(fwpwMModel);

    fwpwMColName = new String[]{"" , "M2" , "M3" , "M4" , "M5" , "M6" , "M7" , "M8" , "M9" };
    fwpwMInfo = new Object[2][9];
    fwpwMInfo[0][0] = "UP";
    fwpwMInfo[1][0] = "DOWN";
    fwpwMModel.setDataVector(fwpwMInfo, fwpwMColName);

    fwpwMView = new JScrollPane(fwpwMTable);
    fwpwMView.setBounds(5,20,595,53);

    fwpwPanel.add(fwpwMView);

    fwpwSModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        return String.class;
      }
      public boolean isCellEditable(int row, int column) {
          return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
      }

    };

    fwpwSTable = new JTable(fwpwSModel);

    fwpwSColName = new String[]{"" , "S2" , "S3" , "S4" , "S5" , "S6" , "S7" , "S8" , "S9" };
    fwpwSInfo = new Object[2][9];
    fwpwSInfo[0][0] = "UP";
    fwpwSInfo[1][0] = "DOWN";
    fwpwSModel.setDataVector(fwpwSInfo, fwpwSColName);

    fwpwSView = new JScrollPane(fwpwSTable);
    fwpwSView.setBounds(5,80,595,53);

    fwpwPanel.add(fwpwSView);

    // RePw Panel ---------------------------------------------------------------

    repwPanel = new JPanel();
    repwPanel.setLayout(null);
    repwPanel.setBorder(BorderFactory.createTitledBorder("Reflected power (KW)"));
    repwPanel.setBounds(380,155,610,140);
    innerPanel.add(repwPanel);

    repwMModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        return String.class;
      }
      public boolean isCellEditable(int row, int column) {
          return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
      }

    };

    repwMTable = new JTable(repwMModel);

    repwMColName = new String[]{"" , "M2" , "M3" , "M4" , "M5" , "M6" , "M7" , "M8" , "M9" };
    repwMInfo = new Object[2][9];
    repwMInfo[0][0] = "UP";
    repwMInfo[1][0] = "DOWN";
    repwMModel.setDataVector(repwMInfo, repwMColName);

    repwMView = new JScrollPane(repwMTable);
    repwMView.setBounds(5,20,595,53);

    repwPanel.add(repwMView);

    repwSModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        return String.class;
      }
      public boolean isCellEditable(int row, int column) {
          return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
      }

    };

    repwSTable = new JTable(repwSModel);

    repwSColName = new String[]{"" , "S2" , "S3" , "S4" , "S5" , "S6" , "S7" , "S8" , "S9" };
    repwSInfo = new Object[2][9];
    repwSInfo[0][0] = "UP";
    repwSInfo[1][0] = "DOWN";
    repwSModel.setDataVector(repwSInfo, repwSColName);

    repwSView = new JScrollPane(repwSTable);
    repwSView.setBounds(5,80,595,53);

    repwPanel.add(repwSView);

    // Global viewers --------------------------------------------

    JLabel fwpwLabel = new JLabel("Ampli Fw Power");
    fwpwLabel.setBounds(5,180,130,25);
    innerPanel.add(fwpwLabel);
    fwpwViewer = new SimpleScalarViewer();
    fwpwViewer.setBounds(135,170,135,40);
    innerPanel.add(fwpwViewer);

    JLabel repwLabel = new JLabel("Ampli Refl Power");
    repwLabel.setBounds(5,230,130,25);
    innerPanel.add(repwLabel);
    repwViewer = new SimpleScalarViewer();
    repwViewer.setBounds(135,220,135,40);
    innerPanel.add(repwViewer);

    // Models ----------------------------------------------------

    attList = new AttributeList();
    attList.addErrorListener(MainPanel.errWin);

    try {

      fwpwSpectrumModel = (INumberSpectrum)attList.add(devName+"/fwpw");
      fwpwSpectrumModel.addSpectrumListener(this);
      repwSpectrumModel = (INumberSpectrum)attList.add(devName+"/repw");
      repwSpectrumModel.addSpectrumListener(this);
      driverSpectrumModel = (INumberSpectrum)attList.add(devName+"/DriverMeas");
      driverSpectrumModel.addSpectrumListener(this);
      fwpwViewer.setModel((INumberScalar) attList.add(domain + "/ssa-adc/" + traName + "/Output_Fw_Pw"));
      repwViewer.setModel((INumberScalar)attList.add(domain + "/ssa-adc/" + traName + "/Output_Refl_Pw"));

    } catch( ConnectionException e) {
    }

    attList.setRefreshInterval(2000);
    attList.startRefresher();

    setContentPane(innerPanel);
    setTitle("RF Power panel [" + devName + "]");

  }

  private String formatDouble(double value) {

    return String.format("%.3f", value);

  }

  public void actionPerformed(ActionEvent evt) {

    Object src = evt.getSource();

    if( src==dismissBtn ) {
      attList.stopRefresher();
      setVisible(false);
    }

  }

  public void stateChange(AttributeStateEvent evt) {

  }

  public void errorChange(ErrorEvent evt) {

    Object src = evt.getSource();

    if( src==fwpwSpectrumModel ) {

      for(int i=0;i<8;i++) {
        fwpwMInfo[0][i+1] = "---";
        fwpwMInfo[1][i+1] = "---";
        fwpwSInfo[0][i+1] = "---";
        fwpwSInfo[1][i+1] = "---";
      }

      SwingUtilities.invokeLater(new Runnable() {
        public void run() {
          fwpwMModel.setDataVector(fwpwMInfo, fwpwMColName);
          fwpwSModel.setDataVector(fwpwSInfo, fwpwSColName);
        }
      });

    } else if( src==repwSpectrumModel ) {

          for(int i=0;i<8;i++) {
            repwMInfo[0][i+1] = "---";
            repwMInfo[1][i+1] = "---";
            repwSInfo[0][i+1] = "---";
            repwSInfo[1][i+1] = "---";
          }

          SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              repwMModel.setDataVector(repwMInfo, repwMColName);
              repwSModel.setDataVector(repwSInfo, repwSColName);
            }
          });

    } else if( src==driverSpectrumModel ) {

      // Pre driver
      driverInfo[0][1] = "---";
      driverInfo[0][2] = "---";
      driverInfo[0][3] = "---";

      // M-UP
      driverInfo[1][1] = "---";
      driverInfo[1][2] = "---";
      driverInfo[1][3] = "---";

      // M-DOWN
      driverInfo[2][1] = "---";
      driverInfo[2][2] = "---";
      driverInfo[2][3] = "---";

      // S-UP
      driverInfo[3][1] = "---";
      driverInfo[3][2] = "---";
      driverInfo[3][3] = "---";

      // S-DOWN
      driverInfo[4][1] = "---";
      driverInfo[4][2] = "---";
      driverInfo[4][3] = "---";

      SwingUtilities.invokeLater(new Runnable() {
        public void run() {
          driverModel.setDataVector(driverInfo, driverColName);
        }
      });

    }

  }

  public void spectrumChange(NumberSpectrumEvent evt) {

    Object src = evt.getSource();

    if( src==fwpwSpectrumModel ) {

      double[] values = evt.getValue();

      for(int i=0;i<8;i++) {
        fwpwMInfo[0][i+1] = formatDouble(values[i]);
        fwpwMInfo[1][i+1] = formatDouble(values[i+16]);
        fwpwSInfo[0][i+1] = formatDouble(values[i+8]);
        fwpwSInfo[1][i+1] = formatDouble(values[i+24]);
      }

      SwingUtilities.invokeLater(new Runnable() {
        public void run() {
          fwpwMModel.setDataVector(fwpwMInfo, fwpwMColName);
          fwpwSModel.setDataVector(fwpwSInfo, fwpwSColName);
        }
      });

    } else if( src==repwSpectrumModel ) {

          double[] values = evt.getValue();

          for(int i=0;i<8;i++) {
            repwMInfo[0][i+1] = formatDouble(values[i]);
            repwMInfo[1][i+1] = formatDouble(values[i+16]);
            repwSInfo[0][i+1] = formatDouble(values[i+8]);
            repwSInfo[1][i+1] = formatDouble(values[i+24]);
          }

          SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              repwMModel.setDataVector(repwMInfo, repwMColName);
              repwSModel.setDataVector(repwSInfo, repwSColName);
            }
          });

    } else if( src==driverSpectrumModel ) {

      double[] values = evt.getValue();

      // Pre driver
      driverInfo[0][1] = formatDouble(values[0]);
      driverInfo[0][2] = formatDouble(values[1]);
      driverInfo[0][3] = formatDouble(values[2]);

      // M-UP
      driverInfo[1][1] = formatDouble(values[3]);
      driverInfo[1][2] = formatDouble(values[4]);
      driverInfo[1][3] = formatDouble(values[5]);

      // M-DOWN
      driverInfo[2][1] = formatDouble(values[6]);
      driverInfo[2][2] = formatDouble(values[7]);
      driverInfo[2][3] = formatDouble(values[8]);

      // S-UP
      driverInfo[3][1] = formatDouble(values[9]);
      driverInfo[3][2] = formatDouble(values[10]);
      driverInfo[3][3] = formatDouble(values[11]);

      // S-DOWN
      driverInfo[4][1] = formatDouble(values[12]);
      driverInfo[4][2] = formatDouble(values[13]);
      driverInfo[4][3] = formatDouble(values[14]);

      SwingUtilities.invokeLater(new Runnable() {
        public void run() {
          driverModel.setDataVector(driverInfo, driverColName);
        }
      });

    }

  }

  public void showDialog() {

    attList.startRefresher();
    setVisible(true);

  }

  public void clearModel() {

    if( fwpwSpectrumModel!=null ) fwpwSpectrumModel.removeSpectrumListener(this);
    if( repwSpectrumModel!=null ) repwSpectrumModel.removeSpectrumListener(this);
    fwpwViewer.clearModel();
    repwViewer.clearModel();

    attList.removeErrorListener(MainPanel.errWin);
    attList.clear();
    attList.stopRefresher();

  }


}
