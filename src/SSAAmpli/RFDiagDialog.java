package SSAAmpli;

import fr.esrf.TangoApi.events.TangoArchive;
import fr.esrf.TangoDs.TangoConst;
import fr.esrf.tangoatk.core.*;
import fr.esrf.tangoatk.core.attribute.NumberImage;
import fr.esrf.tangoatk.core.attribute.NumberScalar;
import fr.esrf.tangoatk.core.attribute.NumberSpectrum;
import fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor;
import fr.esrf.tangoatk.widget.attribute.NumberSpectrumViewer;
import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class StatCellRenderer extends JTextField implements TableCellRenderer {

  private final static Color LIGHT_BLUE = new Color(220,220,255);
  private final static Font  TABLE_FONT = new Font("Dialog",Font.PLAIN,12);
  private final static Font  TABLE_FONT_BOLD = new Font("Dialog",Font.BOLD,12);

  private boolean firstLineBold;

  public StatCellRenderer() {
    firstLineBold = true;
    setEditable(false);
    setOpaque(true);
  }

  public StatCellRenderer(boolean firstLineBold) {
    this.firstLineBold = firstLineBold;
    setEditable(false);
    setOpaque(true);
  }

  public Component getTableCellRendererComponent(JTable table,Object value,
												boolean isSelected, boolean hasFocus, int row, int column) {

    if (value instanceof String) {

      if( firstLineBold ) {
        if(row==0)
          setFont(TABLE_FONT_BOLD);
        else
          setFont(TABLE_FONT);
      }

      int state = Integer.parseInt(((String) value).substring(0, 1));
      setText(((String) value).substring(1));
      switch(state) {
        case 0:
          setBackground(Color.WHITE);
          break;
        case 1:
          setBackground(Color.ORANGE);
          break;
        case 2:
          setBackground(Color.RED);
          break;
        case 3:
          setBackground(LIGHT_BLUE);
          break;
        case 4:
          setBackground(Color.MAGENTA);
          break;
        case 5:
          setBackground(Color.LIGHT_GRAY);
          break;

      }
    }
    else
      setText("");
    return this;
  }
}


/**
 * RF diagnostic panel
 */
public class RFDiagDialog extends JFrame implements ActionListener, IImageListener, INumberScalarListener, IJLChartListener {

  private JPanel innerPanel;
  private JPanel btnPanel;
  private JButton dismissBtn;
  private JButton startstopBtn;
  private JTabbedPane tabPane;


  // Spectrum viewer
  NumberSpectrumViewer      tempViewer;
  NumberSpectrumViewer      loadTempViewer;
  NumberSpectrumViewer      currentViewer;

  // Statistic panel
  private JPanel            statPanel;
  private JPanel            statSetterPanel;
  NumberScalarWheelEditor   xSigmaTransTempSetter;
  NumberScalarWheelEditor   offsetTransTempSetter;
  NumberScalarWheelEditor   xSigmaLoadTempSetter;
  NumberScalarWheelEditor   offsetLoadTempSetter;
  NumberScalarWheelEditor   xSigmaCurrentSetter;
  NumberScalarWheelEditor   offsetCurrentSetter;
  private JTable            statTable;
  private DefaultTableModel statModel;
  private JScrollPane       statView;
  private String[]          statColName;
  private StatCellRenderer  tableCellRenderer;

  // Power panel
  private JPanel            powerPanel;
  private JPanel            powerSetterPanel;
  NumberScalarWheelEditor   xSigmaFwPwSetter;
  NumberScalarWheelEditor   offsetFwPwSetter;
  NumberScalarWheelEditor   xSigmaRePwSetter;
  NumberScalarWheelEditor   offsetRePwSetter;
  private JTable            powerTable;
  private DefaultTableModel powerModel;
  private JScrollPane       powerView;
  private String[]          powerColName;

  // Missing driver panel
  private JTable            missingDriverTable;
  private DefaultTableModel missingDriverModel;
  private JScrollPane       missingDriverView;
  private String[]          missingDriverColName;

  // Missing module panel
  private JTable            missingTable;
  private DefaultTableModel missingModel;
  private JScrollPane       missingView;
  private String[]          missingColName;

  // Models
  private AttributeList     attList;
  private NumberScalar      tempAvgModel = null;
  private NumberScalar      loadTempAvgModel = null;
  private NumberScalar      currentAvgModel = null;
  private NumberImage       statImageModel = null;
  private NumberScalar      fwpwAvgModel = null;
  private NumberScalar      repwAvgModel = null;
  private NumberImage       powerImageModel = null;
  private NumberImage       missingImageModel = null;
  private NumberImage       missingDriverImageModel = null;

  // Values
  private Object[][]        statInfo;
  private double[][]        alarms;
  private double            tempAvg = Double.NaN;
  private double            loadTempAvg = Double.NaN;
  private double            currentAvg = Double.NaN;

  private Object[][]        powerInfo;
  private double[][]        powers;
  private double            fwpwAvg = Double.NaN;
  private double            repwAvg = Double.NaN;

  private Object[][]        missingInfo;
  private double[][]        missing;

  private Object[][]        missingDriverInfo;
  private double[][]        missingDriver;

  RFDiagDialog(String devName) {

    // Layout panels
    innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());

    btnPanel = new JPanel();
    btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    innerPanel.add(btnPanel,BorderLayout.SOUTH);

    startstopBtn = new JButton("Stop Refresher");
    startstopBtn.addActionListener(this);
    btnPanel.add(startstopBtn);

    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    btnPanel.add(dismissBtn);

    // Alarm Table

    statPanel = new JPanel();
    statPanel.setLayout(new BorderLayout());

    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.BOTH;

    statSetterPanel = new JPanel();
    statSetterPanel.setLayout(new GridBagLayout());

    JLabel xsigLabel = new JLabel("X Sigma");
    gbc.gridx = 0;
    gbc.gridy = 1;
    statSetterPanel.add(xsigLabel,gbc);

    JLabel offsetLabel = new JLabel("Offset");
    gbc.gridx = 0;
    gbc.gridy = 2;
    statSetterPanel.add(offsetLabel,gbc);

    JLabel transTempLabel = new JLabel("Trans Temp");
    transTempLabel.setHorizontalAlignment(JLabel.CENTER);
    gbc.gridx = 1;
    gbc.gridy = 0;
    statSetterPanel.add(transTempLabel,gbc);

    JLabel loadTempLabel = new JLabel("Load Temp");
    loadTempLabel.setHorizontalAlignment(JLabel.CENTER);
    gbc.gridx = 2;
    gbc.gridy = 0;
    statSetterPanel.add(loadTempLabel,gbc);

    JLabel currentLabel = new JLabel("Current");
    currentLabel.setHorizontalAlignment(JLabel.CENTER);
    gbc.gridx = 3;
    gbc.gridy = 0;
    statSetterPanel.add(currentLabel,gbc);

    xSigmaTransTempSetter = new NumberScalarWheelEditor();
    xSigmaTransTempSetter.setBackground(getBackground());
    gbc.gridx = 1;
    gbc.gridy = 1;
    statSetterPanel.add(xSigmaTransTempSetter,gbc);

    offsetTransTempSetter = new NumberScalarWheelEditor();
    offsetTransTempSetter.setBackground(getBackground());
    gbc.gridx = 1;
    gbc.gridy = 2;
    statSetterPanel.add(offsetTransTempSetter,gbc);

    xSigmaLoadTempSetter = new NumberScalarWheelEditor();
    xSigmaLoadTempSetter.setBackground(getBackground());
    gbc.gridx = 2;
    gbc.gridy = 1;
    statSetterPanel.add(xSigmaLoadTempSetter,gbc);

    offsetLoadTempSetter = new NumberScalarWheelEditor();
    offsetLoadTempSetter.setBackground(getBackground());
    gbc.gridx = 2;
    gbc.gridy = 2;
    statSetterPanel.add(offsetLoadTempSetter,gbc);

    xSigmaCurrentSetter = new NumberScalarWheelEditor();
    xSigmaCurrentSetter.setBackground(getBackground());
    gbc.gridx = 3;
    gbc.gridy = 1;
    statSetterPanel.add(xSigmaCurrentSetter,gbc);

    offsetCurrentSetter = new NumberScalarWheelEditor();
    offsetCurrentSetter.setBackground(getBackground());
    gbc.gridx = 3;
    gbc.gridy = 2;
    statSetterPanel.add(offsetCurrentSetter,gbc);

    statModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        return String.class;
      }
      public boolean isCellEditable(int row, int column) {
          return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
      }

    };
    statTable = new JTable(statModel);
    statView = new JScrollPane(statTable);

    statColName = new String[]{"Module" , "T-trans (deg C)" , "T-load (deg C)" , "Current (A)"};
    statInfo = new Object[0][4];
    statModel.setDataVector(statInfo, statColName);

    tableCellRenderer = new StatCellRenderer();
    statTable.setDefaultRenderer(String.class, tableCellRenderer);

    statPanel.add(statSetterPanel,BorderLayout.NORTH);
    statPanel.add(statView,BorderLayout.CENTER);

    // Missing Driver Table

    missingDriverModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        return String.class;
      }
      public boolean isCellEditable(int row, int column) {
          return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
      }

    };
    missingDriverTable = new JTable(missingDriverModel);
    missingDriverView = new JScrollPane(missingDriverTable);

    missingDriverColName = new String[]{"Module" , "T-trans (deg C)" , "T-load (deg C)" , "Current (A)"};
    missingDriverInfo = new Object[0][4];
    missingDriverModel.setDataVector(missingDriverInfo, missingDriverColName);

    missingDriverTable.setDefaultRenderer(String.class, new StatCellRenderer(false));

    // Missing Table
    missingModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        return String.class;
      }
      public boolean isCellEditable(int row, int column) {
          return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
      }

    };
    missingTable = new JTable(missingModel);
    missingView = new JScrollPane(missingTable);

    missingColName = new String[]{"Module" , "State" , "T-trans (deg C)" , "T-load (deg C)" , "Current (A)"};
    missingInfo = new Object[0][5];
    missingModel.setDataVector(missingInfo, missingColName);

    missingTable.setDefaultRenderer(String.class, tableCellRenderer);

    // Power Table

    powerPanel = new JPanel();
    powerPanel.setLayout(new BorderLayout());

    powerSetterPanel = new JPanel();
    powerSetterPanel.setLayout(new GridBagLayout());

    JLabel xsigLabel2 = new JLabel("X Sigma");
    gbc.gridx = 0;
    gbc.gridy = 1;
    powerSetterPanel.add(xsigLabel2,gbc);

    JLabel offsetLabel2 = new JLabel("Offset");
    gbc.gridx = 0;
    gbc.gridy = 2;
    powerSetterPanel.add(offsetLabel2,gbc);

    JLabel fwpwLabel = new JLabel("Forward power");
    fwpwLabel.setHorizontalAlignment(JLabel.CENTER);
    gbc.gridx = 1;
    gbc.gridy = 0;
    powerSetterPanel.add(fwpwLabel,gbc);

    JLabel repwLabel = new JLabel("Reflected power");
    repwLabel.setHorizontalAlignment(JLabel.CENTER);
    gbc.gridx = 2;
    gbc.gridy = 0;
    powerSetterPanel.add(repwLabel,gbc);

    xSigmaFwPwSetter = new NumberScalarWheelEditor();
    xSigmaFwPwSetter.setBackground(getBackground());
    gbc.gridx = 1;
    gbc.gridy = 1;
    powerSetterPanel.add(xSigmaFwPwSetter,gbc);

    offsetFwPwSetter = new NumberScalarWheelEditor();
    offsetFwPwSetter.setBackground(getBackground());
    gbc.gridx = 1;
    gbc.gridy = 2;
    powerSetterPanel.add(offsetFwPwSetter,gbc);

    xSigmaRePwSetter = new NumberScalarWheelEditor();
    xSigmaRePwSetter.setBackground(getBackground());
    gbc.gridx = 2;
    gbc.gridy = 1;
    powerSetterPanel.add(xSigmaRePwSetter,gbc);

    offsetRePwSetter = new NumberScalarWheelEditor();
    offsetRePwSetter.setBackground(getBackground());
    gbc.gridx = 2;
    gbc.gridy = 2;
    powerSetterPanel.add(offsetRePwSetter,gbc);

    powerModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        return String.class;
      }
      public boolean isCellEditable(int row, int column) {
          return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
      }

    };
    powerTable = new JTable(powerModel);
    powerView = new JScrollPane(powerTable);

    powerColName = new String[]{"Cold Plate" , "State" , "Fw Power" , "Re Power"};
    powerInfo = new Object[0][4];
    powerModel.setDataVector(powerInfo, powerColName);

    powerTable.setDefaultRenderer(String.class, tableCellRenderer);

    powerPanel.add(powerSetterPanel,BorderLayout.NORTH);
    powerPanel.add(powerView,BorderLayout.CENTER);

    // Spectrum viewer
    tempViewer = new NumberSpectrumViewer();
    tempViewer.setJLChartListener(this);
    tempViewer.getY1Axis().getDataView(0).setMarker(JLDataView.MARKER_DOT);
    tempViewer.getY1Axis().getDataView(0).setLineWidth(0);

    loadTempViewer = new NumberSpectrumViewer();
    loadTempViewer.setJLChartListener(this);
    loadTempViewer.getY1Axis().getDataView(0).setMarker(JLDataView.MARKER_DOT);
    loadTempViewer.getY1Axis().getDataView(0).setLineWidth(0);

    currentViewer = new NumberSpectrumViewer();
    currentViewer.setJLChartListener(this);
    currentViewer.getY1Axis().getDataView(0).setMarker(JLDataView.MARKER_DOT);
    currentViewer.getY1Axis().getDataView(0).setLineWidth(0);

    // Tabbed pane
    tabPane = new JTabbedPane();
    tabPane.setPreferredSize(new Dimension(800, 500));
    tabPane.add("Missing module",missingView);
    tabPane.add("Missing driver",missingDriverView);
    tabPane.add("Transistor alarms",statPanel);
    tabPane.add("Power alarms",powerPanel);
    tabPane.add("Temp chart",tempViewer);
    tabPane.add("Load Temp chart",loadTempViewer);
    tabPane.add("Current chart",currentViewer);
    innerPanel.add(tabPane,BorderLayout.CENTER);

    // Models
    attList = new AttributeList();
    attList.addErrorListener(MainPanel.errWin);

    try {

      statImageModel = (NumberImage)attList.add(devName+"/AlarmModules");
      statImageModel.addImageListener(this);
      tempAvgModel = (NumberScalar)attList.add(devName+"/AvgTemp");
      tempAvgModel.addNumberScalarListener(this);
      loadTempAvgModel = (NumberScalar)attList.add(devName+"/AvgLoadTemp");
      loadTempAvgModel.addNumberScalarListener(this);
      currentAvgModel = (NumberScalar)attList.add(devName+"/AvgCurrent");
      currentAvgModel.addNumberScalarListener(this);

      NumberScalar xsigTransTempModel = (NumberScalar)attList.add(devName+"/XSigmaTransTemp");
      xSigmaTransTempSetter.setModel(xsigTransTempModel);
      NumberScalar xsigLoadTempModel = (NumberScalar)attList.add(devName+"/XSigmaLoadTemp");
      xSigmaLoadTempSetter.setModel(xsigLoadTempModel);
      NumberScalar xsigCurrentModel = (NumberScalar)attList.add(devName+"/XSigmaCurrent");
      xSigmaCurrentSetter.setModel(xsigCurrentModel);
      NumberScalar offsetTransTempModel = (NumberScalar)attList.add(devName+"/OffsetTransTemp");
      offsetTransTempSetter.setModel(offsetTransTempModel);
      NumberScalar offsetLoadTempModel = (NumberScalar)attList.add(devName+"/OffsetLoadTemp");
      offsetLoadTempSetter.setModel(offsetLoadTempModel);
      NumberScalar offsetCurrentModel = (NumberScalar)attList.add(devName+"/OffsetCurrent");
      offsetCurrentSetter.setModel(offsetCurrentModel);

      powerImageModel = (NumberImage)attList.add(devName+"/AlarmPowers");
      powerImageModel.addImageListener(this);
      fwpwAvgModel = (NumberScalar)attList.add(devName+"/AvgFwPw");
      fwpwAvgModel.addNumberScalarListener(this);
      repwAvgModel = (NumberScalar)attList.add(devName+"/AvgRePw");
      repwAvgModel.addNumberScalarListener(this);

      NumberScalar xsigFwPwModel = (NumberScalar)attList.add(devName+"/XSigmaFwPw");
      xSigmaFwPwSetter.setModel(xsigFwPwModel);
      NumberScalar xsigRePwModel = (NumberScalar)attList.add(devName+"/XSigmaRePw");
      xSigmaRePwSetter.setModel(xsigRePwModel);
      NumberScalar offsetFwPwModel = (NumberScalar)attList.add(devName+"/OffsetFwPw");
      offsetFwPwSetter.setModel(offsetFwPwModel);
      NumberScalar offsetRePwModel = (NumberScalar)attList.add(devName+"/OffsetRePw");
      offsetRePwSetter.setModel(offsetRePwModel);

      missingImageModel = (NumberImage)attList.add(devName+"/MissingModules");
      missingImageModel.addImageListener(this);

      missingDriverImageModel = (NumberImage)attList.add(devName+"/MissingDrivers");
      missingDriverImageModel.addImageListener(this);

      NumberSpectrum tempsModel = (NumberSpectrum)attList.add(devName+"/Temperatures");
      tempViewer.setModel(tempsModel);

      NumberSpectrum loadTempsModel = (NumberSpectrum)attList.add(devName+"/LoadTemperatues");
      loadTempViewer.setModel(loadTempsModel);

      NumberSpectrum currentsModel = (NumberSpectrum)attList.add(devName+"/Currents");
      currentViewer.setModel(currentsModel);

    } catch( ConnectionException e) {
    }

    attList.setRefreshInterval(2000);
    
    attList.startRefresher();
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        clearModel();
      }
    });
    

    setContentPane(innerPanel);
    setTitle("RF diagnostics [" + devName + "]");

  }
  
  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if(src==dismissBtn) {
      setVisible(false);
      clearModel();
    } else if(src==startstopBtn) {
      if( attList.isRefresherStarted() ) {
        attList.stopRefresher();
        startstopBtn.setText("Start refresher");
      } else {
        attList.startRefresher();
        startstopBtn.setText("Stop refresher");
      }
    }

  }

  public void clearModel() {

    xSigmaTransTempSetter.setModel(null);
    xSigmaLoadTempSetter.setModel(null);
    xSigmaCurrentSetter.setModel(null);
    offsetTransTempSetter.setModel(null);
    offsetLoadTempSetter.setModel(null);
    offsetCurrentSetter.setModel(null);

    if(statImageModel!=null) statImageModel.removeImageListener(this);
    if(tempAvgModel!=null) tempAvgModel.removeNumberScalarListener(this);
    if(loadTempAvgModel!=null) loadTempAvgModel.removeNumberScalarListener(this);
    if(currentAvgModel!=null) currentAvgModel.removeNumberScalarListener(this);

    xSigmaFwPwSetter.setModel(null);
    xSigmaRePwSetter.setModel(null);
    offsetFwPwSetter.setModel(null);
    offsetRePwSetter.setModel(null);

    if(powerImageModel!=null) powerImageModel.removeImageListener(this);
    if(fwpwAvgModel!=null) fwpwAvgModel.removeNumberScalarListener(this);
    if(repwAvgModel!=null) repwAvgModel.removeNumberScalarListener(this);

    if(missingImageModel!=null) missingImageModel.removeImageListener(this);
    if(missingDriverImageModel!=null) missingDriverImageModel.removeImageListener(this);

    tempViewer.setModel(null);
    loadTempViewer.setModel(null);
    currentViewer.setModel(null);

    attList.stopRefresher();
    attList.removeErrorListener(MainPanel.errWin);
    attList.clear();

  }

  public void stateChange(AttributeStateEvent evt) {}

  public void errorChange(ErrorEvent evt) {

    Object src = evt.getSource();

    if( src==tempAvgModel ) {
      tempAvg = Double.NaN;
    } else if ( src==loadTempAvgModel ) {
      loadTempAvg = Double.NaN;
    } else if ( src==currentAvgModel ) {
      currentAvg = Double.NaN;
    } else if( src==statImageModel ) {
      alarms = null;
    } else if ( src==repwAvgModel ) {
      repwAvg = Double.NaN;
    } else if ( src==fwpwAvgModel ) {
      fwpwAvg = Double.NaN;
    } else if( src==powerImageModel ) {
      powers = null;
    } else if ( src==missingImageModel ) {
      missing = null;
    } else if ( src==missingDriverImageModel ) {
      missingDriver = null;
    }

    refreshTable();

  }

  public void numberScalarChange(NumberScalarEvent evt) {

    Object src = evt.getSource();

    if( src==tempAvgModel ) {
      tempAvg = evt.getValue();
    } else if ( src==loadTempAvgModel ) {
      loadTempAvg = evt.getValue();
    } else if ( src==currentAvgModel ) {
      currentAvg = evt.getValue();
    } else if ( src==fwpwAvgModel ) {
      fwpwAvg = evt.getValue();
    } else if ( src==repwAvgModel ) {
      repwAvg = evt.getValue();
    }

    refreshTable();

  }

  public void imageChange(NumberImageEvent evt) {

    Object src = evt.getSource();

    if( src==statImageModel ) {
      alarms = evt.getValue();
    } else if ( src==powerImageModel ) {
      powers = evt.getValue();
    } else if ( src==missingImageModel ) {
      missing = evt.getValue();
    } else if ( src==missingDriverImageModel ) {
      missingDriver = evt.getValue();
    }

    refreshTable();

  }

  public String[] clickOnChart(JLChartEvent evt) {

    String mName = getModuleName((int)evt.getXValue());
    String attName = evt.getDataView().getName();
    String value = formatDouble(evt.getYValue());

    return new String[]{attName,mName,value};

  }

  private String formatDouble(double value) {

    return String.format("%.3f", value);

  }

  private String getDriverName(int driver) {

    switch(driver) {
      case 0:
        return "Pre-Driver";
      case 1:
        return "Driver M-Up";
      case 2:
        return "Driver M-Down";
      case 3:
        return "Driver S-Up";
      case 4:
        return "Driver S-Down";
      default:
        return "Unknown";
    }

  }

  private String getModuleName(int module) {

    if( module>127 ) {

      // Slave
      int cp = ((module-128) / 16)+2;
      int tr = ((module-128) % 16)+1;
      return "S" + Integer.toString(cp) + "-" + Integer.toString(tr);

    } else {

      // Master
      int cp = ((module) / 16)+2;
      int tr = ((module) % 16)+1;
      return "M" + Integer.toString(cp) + "-" + Integer.toString(tr);

    }


  }

  public void showDialog() {

    startstopBtn.setText("Stop Refresher");
    attList.startRefresher();
    setVisible(true);

  }


  private String getCPName(int cp) {

    if( cp>15 ) {

      // Down
      int cpu = ((cp-16) / 8);
      int cpi = ((cp-16) % 8)+2;
      if( cpu==0 )
        return "CP M" + Integer.toString(cpi) + "-Down";
      else
        return "CP S" + Integer.toString(cpi) + "-Down";

    } else {

      // Up
      int cpu = ((cp) / 8);
      int cpi = ((cp) % 8)+2;
      if( cpu==0 )
        return "CP M" + Integer.toString(cpi) + "-Up";
      else
        return "CP S" + Integer.toString(cpi) + "-Up";

    }

  }

  private void refreshTable() {

    synchronized (this) {

      int lgth = 1;
      if(alarms!=null) lgth = alarms.length + 1;

      statInfo = new Object[lgth][4];

      // Average
      statInfo[0][0] = "3Average";
      statInfo[0][1] = "3" + formatDouble(tempAvg);
      statInfo[0][2] = "3" + formatDouble(loadTempAvg);
      statInfo[0][3] = "3" + formatDouble(currentAvg);

      // Alarms
      for (int i = 0; i < lgth-1; i++) {

        statInfo[i+1][0] = "0" + getModuleName((int) alarms[i][0]);
        statInfo[i+1][1] = Integer.toString((int) alarms[i][5]) + formatDouble(alarms[i][2]);
        statInfo[i+1][2] = Integer.toString((int) alarms[i][6]) + formatDouble(alarms[i][3]);
        statInfo[i+1][3] = Integer.toString((int) alarms[i][7]) + formatDouble(alarms[i][4]);

      }

      lgth = 1;
      if(powers!=null) lgth = powers.length + 1;

      powerInfo = new Object[lgth][4];

      // Average
      powerInfo[0][0] = "3Average";
      powerInfo[0][1] = "3";
      powerInfo[0][2] = "3" + formatDouble(fwpwAvg);
      powerInfo[0][3] = "3" + formatDouble(repwAvg);

      // Powers
      for (int i = 0; i < lgth-1; i++) {

        powerInfo[i+1][0] = "0" + getCPName((int) powers[i][0]);
        int state = (int) powers[i][1];
        switch(state) {
          case 0:
            powerInfo[i+1][1] = "0ON";
            break;
          case 1:
            powerInfo[i+1][1] = "1ALARM";
            break;
          case 2:
            powerInfo[i+1][1] = "2OFF";
            break;
          case 4:
            powerInfo[i+1][1] = "4DISABLE";
            break;
          case 5:
            powerInfo[i+1][1] = "5UNKNOWN";
            break;
          case 6:
            powerInfo[i+1][1] = "2FAULT";
            break;
        }

        powerInfo[i+1][2] = Integer.toString((int) powers[i][4]) + formatDouble(powers[i][2]);
        powerInfo[i+1][3] = Integer.toString((int) powers[i][5]) + formatDouble(powers[i][3]);

      }

      lgth = 1;
      if(missing!=null) lgth = missing.length + 1;

      missingInfo = new Object[lgth][5];

      // Average
      missingInfo[0][0] = "3Average";
      missingInfo[0][1] = "3";
      missingInfo[0][2] = "3" + formatDouble(tempAvg);
      missingInfo[0][3] = "3" + formatDouble(loadTempAvg);
      missingInfo[0][4] = "3" + formatDouble(currentAvg);

      // Missing modules
      for (int i = 0; i < lgth-1; i++) {

        missingInfo[i+1][0] = "0" + getModuleName((int) missing[i][0]);
        int state = (int) missing[i][1];

        missingInfo[i+1][1] = Integer.toString((int) missing[i][5]) + TangoConst.Tango_DevStateName[(int)state];
        missingInfo[i+1][2] = Integer.toString((int) missing[i][6]) + formatDouble(missing[i][2]);
        missingInfo[i+1][3] = Integer.toString((int) missing[i][7]) + formatDouble(missing[i][3]);
        missingInfo[i+1][4] = Integer.toString((int) missing[i][8]) + formatDouble(missing[i][4]);

      }

      lgth = 0;
      if( missingDriver!=null ) lgth = missingDriver.length;
      missingDriverInfo = new Object[lgth][5];

      // Missing drivers
      for (int i = 0; i < lgth; i++) {

        missingDriverInfo[i][0] = "0" + getDriverName((int) missingDriver[i][0]);
        missingDriverInfo[i][1] = Integer.toString((int) missingDriver[i][4]) + formatDouble(missingDriver[i][1]);
        missingDriverInfo[i][2] = Integer.toString((int) missingDriver[i][5]) + formatDouble(missingDriver[i][2]);
        missingDriverInfo[i][3] = Integer.toString((int) missingDriver[i][6]) + formatDouble(missingDriver[i][3]);

      }

      SwingUtilities.invokeLater(new Runnable() {
        public void run() {
          statModel.setDataVector(statInfo, statColName);
          powerModel.setDataVector(powerInfo,powerColName);
          missingModel.setDataVector(missingInfo,missingColName);
          missingDriverModel.setDataVector(missingDriverInfo,missingDriverColName);
        }
      });

    }

  }

}

